from django.urls import path
from .views import create_task, show_my_tasks


urlpatterns = [
    # path("", create_task, name="create_task"),
    path("create/", create_task, name="create_task"),
    path("tasks/", show_my_tasks, name="show_my_tasks"),
]
