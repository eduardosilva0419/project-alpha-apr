from django.shortcuts import render, redirect
from .models import Task
from .forms import TaskForm
from django.contrib.auth.decorators import login_required

# from .forms import TaskForm


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            # tasks = form.save()
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    if request.method == "GET":
        show_my_tasks = Task.objects.filter(assignee=request.user)
        context = {
            "show_my_tasks": show_my_tasks,
        }
        return render(request, "mine/list.html", context)
        # if show_my_tasks is None:
        #     return "You have no tasks"
    else:
        return "You have no tasks"
