from django.urls import path

# had also a import of include in here which wasnt required.
from .views import login_user, logout_user, signup_user

# from projects.views import list_projects, show_project, create_project
from django.shortcuts import redirect


def redirects_name(resquest):
    return redirect("home")


urlpatterns = [
    path("login/", login_user, name="login"),
    path("logout/", logout_user, name="logout"),
    path("signup/", signup_user, name="signup"),
    # path("projects/", list_projects, name="projects"),
    # path("projects/", include("projects.urls")),
    # path("<int:id>/", show_project, name="show_project"),
    # path("", redirects_name),
    # path("create/", create_project, name="create_project"),
    # these dont need to live here since this is only requires
    # the URLs related to accounts, which is login/out.
]
