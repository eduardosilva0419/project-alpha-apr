from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm

# from projects.forms import ProjectForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,
    }

    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    owner_project = project.owner.projects.all()
    context = {
        "show_project": project,
        "owner": owner_project,
    }
    return render(request, "projects/details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            # project.name = request.user mistake made with this line. It created a user name for the project name instead.
            # project.description = request.user
            project.owner = request.user
            # dont know if I need the preceding two lines, but will verify if it works.
            # commented out the two lines and nothing broke, telling me they are not needed.
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
